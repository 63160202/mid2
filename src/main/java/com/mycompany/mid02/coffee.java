/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mid02;

/**
 *
 * @author admin
 */
public class coffee extends drink1 {  //สร้างclass coffee แล้วสืบทอดจาก class drink1
    public coffee( int amount, int price) {  //ประกาศ constructor ของ class coffee รับ parameter 2ตัว
        super(amount,price);  //super คือ นำparameter ของ constructor ของ class coffee ไปเก็บไว้ใน attributes ของ class drink1
    }

    @Override
    public void show() { ///สร้าง method show ไม่มีการ returnค่ากลับ เพื่อแสดงข้อมูลดังนี้ ปริมาณ ราคา
        System.out.println("Name: Beer  taste: bitter " +" amount : " + amount +" ML" + " price : " + price);
    }
}


