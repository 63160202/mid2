/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mid02;

/**
 *
 * @author admin
 */
public class drink1 {  //สร้างคลาสชื่อว่าdrink1
    
    protected int amount;  //ประกาศ attributes amount เป็น type int มี modifiers เป็น protected
    protected int price;   //ประกาศ attributes price เป็น type int มี modifiers เป็น protected


    public drink1(int amount, int price) { //ประกาศ constructor ของ class drink1 รับมา 2 parameter
        this.amount = amount;  //เซ็ต parameter amount เข้าไปใน attributes amount
        this.price = price;  //เซ็ต parameter price เข้าไปใน attributes price
    }
    
    public void show() {   //สร้าง method show ไม่มีการ returnค่ากลับ เพื่อแสดงข้อมูลดังนี้ ปริมาณ ราคา
        System.out.println(" amount : " + amount +" ML" + " price : " + price  );
    }
    
     //(overload)
    public int calculateprice() {  //สร้าง method calculateprice มีการ returnค่าprice เป็น int  (overload)
       return price;
    }
    //(overload)  methodชื่อเหมือนกันแต่รับค่าparameterต่างกัน
    public int calculateprice(int buyer) {   //สร้าง method calculateprice มีการ returnค่าprice เป็น int  (overload)
        return buyer*price;
    }
   


}

