/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mid02;

/**
 *
 * @author admin
 */
public class tastdrink1 {
    public static void main(String[] args) {
        person person1 = new person("deep"); //สร้าง object ชื่อ person1 จาก class person โดยรับparameter เอาไปไว้ในconstructor ของ object person1
        System.out.println("====================================");
        beer Beer1 = new beer(500,180); //สร้าง object ชื่อ person1 จาก class beer โดยรับparameter เอาไปไว้ในconstructor ของ object Beer1
        Beer1.show();  //ใช้ object Beer1 เรียกใช้ methodshow เพื่อโชว์ ชื่อน้ำ รสชาติ ปริมาณ ราคา
        person1.numamount(2);    //นับจำนวนของสินค้าทั้งหมด
        person1.showamount();   //ใช้ object person1 เรียกใช้ methodshowamount เพื่อโชว์จำนวนสินค้า
        person1.numprice(Beer1.calculateprice(2)); //ใช้object Beer1 เรียกใช้method calculateprice ส่งarguments แล้วนำผลลัพท์ที่ได้ไปใส่ในobject person1 เรียกใช้methodnumprice
        System.out.println("total : " + Beer1.calculateprice(2));   //แสดงราคารวมทั้งหมด
        System.out.println("====================================");
        softdrink softdrink1 = new softdrink(2000,45);//สร้าง object ชื่อ softdrink1 จาก class softdrink โดยรับparameter เอาไปไว้ในconstructor ของ object softdrink1
        softdrink1.show();  //ใช้ object softdrink1 เรียกใช้ methodshow เพื่อโชว์ ชื่อน้ำ รสชาติ ปริมาณ ราคา
        person1.numamount(1);  //นับจำนวนของสินค้าทั้งหมด
        person1.showamount();   //ใช้ object person1 เรียกใช้ methodshowamount เพื่อโชว์จำนวนสินค้า
        person1.numprice(softdrink1.calculateprice(1));  //ใช้object softdrink1 เรียกใช้method calculateprice ส่งarguments แล้วนำผลลัพท์ที่ได้ไปใส่ในobject person1 เรียกใช้methodnumprice
        System.out.println("total : " + softdrink1.calculateprice());  //แสดงราคารวมทั้งหมด
        System.out.println("====================================");
        person person2 = new person("wink");   //สร้าง object ชื่อ person2 จาก class person โดยรับparameter เอาไปไว้ในconstructor ของ object person2
        juice juice1 = new juice(1500,100);  //สร้าง object ชื่อ juice1 จาก class juice โดยรับparameter เอาไปไว้ในconstructor ของ object juice1
        juice1.show();   //ใช้ object juice1 เรียกใช้ methodshow เพื่อโชว์ ชื่อน้ำ รสชาติ ปริมาณ ราคา
        person2.numamount(5);  //นับจำนวนของสินค้าทั้งหมด
        person2.showamount();   //ใช้ object person1 เรียกใช้ methodshowamount เพื่อโชว์จำนวนสินค้า
        person2.numprice(juice1.calculateprice(5));  //ใช้object juice1 เรียกใช้method calculateprice ส่งarguments แล้วนำผลลัพท์ที่ได้ไปใส่ในobject person2 เรียกใช้methodnumprice
        System.out.println("total : " + juice1.calculateprice(5));  //แสดงราคารวมทั้งหมด
        System.out.println("====================================");
        coffee coffee1 = new coffee(200,50);  //สร้าง object ชื่อ coffee1 จาก class coffee โดยรับparameter เอาไปไว้ในconstructor ของ object coffee1
        coffee1.show();  //ใช้ object coffee1 เรียกใช้ methodshow เพื่อโชว์ ชื่อน้ำ รสชาติ ปริมาณ ราคา
        person2.numamount(3);  //นับจำนวนของสินค้าทั้งหมด
        person2.showamount();   //ใช้ object person1 เรียกใช้ methodshowamount เพื่อโชว์จำนวนสินค้า
        person2.numprice(coffee1.calculateprice(3));  //ใช้object coffee1 เรียกใช้method calculateprice ส่งarguments แล้วนำผลลัพท์ที่ได้ไปใส่ในobject person2 เรียกใช้methodnumprice
        System.out.println("total : " + coffee1.calculateprice(3));  //แสดงราคารวมทั้งหมด
        System.out.println("====================================");
        
        person1.show();  //ใช้ object person1 เรียกใช้ methodshow เพื่อโชว์ จำนวนปริมาณสินค้าที่ซื้อทั้งหมด , จำนวนราคาการซื้อสินค้าทั้งหมด
        person2.show(); //ใช้ object person2 เรียกใช้ methodshow เพื่อโชว์ ชื่อน้ำ รสชาติ ปริมาณ ราคา
        

    }
}
